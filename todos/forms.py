from django.forms import ModelForm
from todos.models import TodoList


class MakeList(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            "name",
        ]
