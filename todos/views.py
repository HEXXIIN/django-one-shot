from django.shortcuts import render, redirect
from todos.models import TodoItem, TodoList
from todos.forms import MakeList


# Create your views here.
def todo_list_list(request):
    req_list = TodoList.objects.all()
    context = {
        "list_list": req_list,
    }
    return render(request, "todos/lists.html", context)


def todo_list_detail(request, id):
    list_item = TodoList.objects.get(id=id)
    context = {
        "task_object": list_item,
    }
    return render(request, "todos/listdetails.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = MakeList(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")

    else:
        form = MakeList()

    context = {
        "create_new": form,
    }

    return render(request, "todos/create.html", context)
